<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
require '../vendor/autoload.php';
use SquareConnect\Configuration;
use SquareConnect\ApiClient;

class UserController extends Controller
{
    public function index()
    {	
		$access_token = 'YOUR_ACCESS_TOKEN';
		# setup authorization
		$api_config = new Configuration();
		$api_config->setHost("https://connect.squareup.com");
		$api_config->setAccessToken($access_token);
		$api_client = new ApiClient($api_config);
		dd($api_client);
    }
}
